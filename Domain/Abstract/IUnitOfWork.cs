﻿namespace Domain.Abstract
{
    public interface IUnitOfWork
    {
        void Commit();
        IAlbumRepository AlbumRepository { get; }
    }
}