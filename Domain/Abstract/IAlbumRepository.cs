﻿using System.Collections.Generic;
using Domain.Models;

namespace Domain.Abstract
{
    public interface IAlbumRepository
    {
        bool AddNewAlbum(Album album);

        IEnumerable<Album> GetAllAlbums();

        Album GetAlbumById(int id);

        bool RemoveAlbum(Album album);

        bool EditAlbum(Album album);

        bool AddSong(Song song);
    }
}