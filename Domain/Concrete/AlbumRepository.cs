﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Abstract;
using Domain.Models;

namespace Domain.Concrete
{
    public class AlbumRepository : IAlbumRepository
    {
        private readonly ApplicationDbContext _context;

        public AlbumRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool AddNewAlbum(Album album)
        {
            try
            {
                _context.Albums.Add(album);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }

        }

        public IEnumerable<Album> GetAllAlbums()
        {
            return _context.Albums.ToList();
        }

        public Album GetAlbumById(int id)
        {
            return _context.Albums.FirstOrDefault(a => a.Id == id);
        }

        public bool RemoveAlbum(Album album)
        {
            try
            {
                var albumToRemove = _context.Albums.FirstOrDefault(a => a.Id == album.Id);
                _context.Albums.Remove(albumToRemove);
                _context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EditAlbum(Album album)
        {
            var albumToEdit = _context.Albums.FirstOrDefault(a => a.Id == album.Id);

            if (albumToEdit != null)
            {
                albumToEdit.Artist = album.Artist;
                albumToEdit.Genre = album.Genre;
                albumToEdit.Title = album.Title;
                albumToEdit.Year = album.Year;

                _context.Entry(albumToEdit).State = EntityState.Modified;
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool AddSong(Song song)
        {
            if (string.IsNullOrWhiteSpace(song.Title))
                return false;

            var album = _context.Albums.FirstOrDefault(a => a.Id == song.AlbumId);
            if (album != null)
            {
                if (album.Songs != null)
                {
                    album.Songs.Add(song);
                }
                else
                {
                    album.Songs = new List<Song>{song};
                }
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}