﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;

namespace Domain.Models
{
    public class Album
    {
        [Key]
        public int Id { get; set; }

        public string Artist { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }

        [ScriptIgnore]
        public virtual ICollection<Song> Songs { get; set; } 
    }
}