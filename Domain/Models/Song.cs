﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;

namespace Domain.Models
{
    public class Song
    {
        [Key]
        public int Id { get; set;}
        public int AlbumId { get; set; }
        public string Title { get; set; }

        [ForeignKey("AlbumId"), ScriptIgnore]
        public virtual Album Album { get; set; }
    }
}