﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Concrete;
using Domain.Models;
using Newtonsoft.Json;

namespace CDCatalogue.Controllers
{
    public class AlbumsController : Controller
    {
        private readonly IUnitOfWork _uow;

        public AlbumsController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        // GET: Albums
        public ActionResult Index()
        {
            return View(_uow.AlbumRepository.GetAllAlbums());
        }

        public string GetAllAlbums()
        {
            var albums = _uow.AlbumRepository.GetAllAlbums();

            var albumsJson = JsonConvert.SerializeObject(albums, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return albumsJson;
        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = _uow.AlbumRepository.GetAlbumById((int) id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return PartialView(album);
        }

        // GET: Albums/Create
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Create([Bind(Include = "Id,Artist,Title,Genre,Year")] Album album)
        {
            if (ModelState.IsValid)
            {
                _uow.AlbumRepository.AddNewAlbum(album);
                return Json(true);
            }

            return Json(false);
        }

        // GET: Albums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = _uow.AlbumRepository.GetAlbumById((int)id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return PartialView(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public JsonResult Edit([Bind(Include = "Id,Artist,Title,Genre,Year")] Album album)
        {
            if (ModelState.IsValid)
            {
                _uow.AlbumRepository.EditAlbum(album);
                return Json(true);
            }
            return Json(false);
        }

        // GET: Albums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = _uow.AlbumRepository.GetAlbumById((int)id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return PartialView(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        public JsonResult DeleteConfirmed(int id)
        {
            try
            {
                Album album = _uow.AlbumRepository.GetAlbumById(id);
                _uow.AlbumRepository.RemoveAlbum(album);
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }

        public ActionResult AddSong(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Album album = _uow.AlbumRepository.GetAlbumById((int)id);

            Song song = new Song();
            song.Album = album;

            if (album == null)
            {
                return HttpNotFound();
            }
            return PartialView(song);
        }

        [HttpPost]
        public JsonResult AddSong(Song model)
        {
            if (ModelState.IsValid)
            {
                var album = _uow.AlbumRepository.GetAlbumById(model.AlbumId);

                model.Album = album;

                if(_uow.AlbumRepository.AddSong(model))
                    return Json(true);
            }

            return Json(false);
        }
    }
}
