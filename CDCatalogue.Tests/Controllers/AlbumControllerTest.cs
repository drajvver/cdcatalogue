﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CDCatalogue.Controllers;
using Domain.Abstract;
using Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CDCatalogue.Tests.Controllers
{
    [TestClass]
    public class AlbumControllerTest
    {
        [TestMethod]
        public void Index()
        {
            var mock = new Mock<IUnitOfWork>();

            mock.Setup(a => a.AlbumRepository.GetAllAlbums()).Returns(new List<Album>()
            {
                new Album()
                {
                    Artist = "Metallica",
                    Genre = "Thrash Metal",
                    Id = 1,
                    Songs = new List<Song>(),
                    Title = "Black Album",
                    Year = 1991
                }
            });

            var repo = mock.Object;

            AlbumsController controller = new AlbumsController(repo);

            ViewResult result = controller.Index() as ViewResult;
            
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result.Model, typeof(List<Album>));
        }

        [TestMethod]
        public void Create()
        {
            var mock = new Mock<IUnitOfWork>();

            Album album = new Album()
            {
                Artist = "Metallica",
                Genre = "Thrash Metal",
                Id = 1,
                Songs = new List<Song>(),
                Title = "Black Album",
                Year = 1991
            };

            mock.Setup(a => a.AlbumRepository.AddNewAlbum(album)).Returns(true);

            var repo = mock.Object;

            AlbumsController controller = new AlbumsController(repo);

            JsonResult result = controller.Create(album);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Remove()
        {
            var mock = new Mock<IUnitOfWork>();

            Album album = new Album()
            {
                Artist = "Metallica",
                Genre = "Thrash Metal",
                Id = 1,
                Songs = new List<Song>(),
                Title = "Black Album",
                Year = 1991
            };

            mock.Setup(a => a.AlbumRepository.RemoveAlbum(album)).Returns(true);

            var repo = mock.Object;

            AlbumsController controller = new AlbumsController(repo);

            JsonResult result = controller.DeleteConfirmed(album.Id);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Edit()
        {
            var mock = new Mock<IUnitOfWork>();

            Album album = new Album()
            {
                Artist = "Metallica",
                Genre = "Thrash Metal",
                Id = 1,
                Songs = new List<Song>(),
                Title = "Black Album",
                Year = 1991
            };

            mock.Setup(a => a.AlbumRepository.EditAlbum(album)).Returns(true);

            var repo = mock.Object;

            AlbumsController controller = new AlbumsController(repo);

            JsonResult result = controller.Edit(album);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddSong()
        {
            var mock = new Mock<IUnitOfWork>();

            Album album = new Album()
            {
                Artist = "Metallica",
                Genre = "Thrash Metal",
                Id = 1,
                Songs = new List<Song>(),
                Title = "Black Album",
                Year = 1991
            };


            Song song = new Song()
            {
                Album = album,
                AlbumId = album.Id,
                Title = "Enter Sandman"
            };

            mock.Setup(a => a.AlbumRepository.AddSong(song)).Returns(true);

            var repo = mock.Object;

            AlbumsController controller = new AlbumsController(repo);

            JsonResult result = controller.AddSong(song);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void AddSong_WillFailOnNullAlbum()
        {
            try
            {
                var mock = new Mock<IUnitOfWork>();

                Album album = null;

                Song song = new Song()
                {
                    Album = album,
                    AlbumId = album.Id,
                    Title = "Enter Sandman"
                };

                mock.Setup(a => a.AlbumRepository.AddSong(song)).Returns(true);

                var repo = mock.Object;

                AlbumsController controller = new AlbumsController(repo);

                JsonResult result = controller.AddSong(song);

                Assert.Fail();
            }
            catch(Exception ex)
            {
                
            }
        }
    }
}
